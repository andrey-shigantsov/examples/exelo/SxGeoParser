/* 
 * File:   SxGeoParser.h
 */

#ifndef SXGEOPARSER_H
#define SXGEOPARSER_H

#include <cstdint>

#include <vector>
#include <string>
#include <map>

class SxGeoParser_t
{
public:
  enum errCode_t {errNone, errGeneral, errFileOpen, errRead, errCheck_dbId, errParse_dbUniFormat, errParse_dbUniFormatPDU};
  
  enum dbType_t {dbUniversal = 0, dbCountry = 1, dbCity = 2, dbCountryIp = 11, dbCityIp = 12, dbIpgeobase = 21};
  enum dbEncoding_t {dbUtf8 = 0, dbLatin1 = 1, dbCP1251 = 2};
  struct dbHeader_t
  {
    bool isValid;
    uint8_t ver;
    uint32_t createTimestamp;
    uint8_t type;
    uint8_t encoding;
    uint8_t elCount_first; /* Элементов в индексе первых байт */
    uint16_t elCount; /* Элементов в основном индексе */
    uint16_t blCount; /* Блоков в одном элементе индекса */
    uint32_t rangesCount; /* Количество диапазонов */
    uint8_t blIdSize; /* Размер ID-блока в байтах (1 для стран, 3 для городов) */
    uint16_t recRegionMaxSize; /* Максимальный размер записи региона */
    uint16_t recCityMaxSize; /* Максимальный размер записи города */
    uint32_t ctlgRegionSize; /* Размер справочника регионов */
    uint32_t ctlgCitySize; /* Размер справочника городов */
    uint16_t recCountryMaxSize; /* Максимальный размер записи страны */
    uint32_t ctlgCountrySize; /* Размер справочника стран */
    uint16_t frmtDescSize; /* Размер описания формата упаковки города/региона/страны */
    std::vector<std::string> frmtDesc; /* Описания формата упаковки города/региона/страны */
  };
  struct dbRange_t
  {
    uint32_t addr;
    uint32_t geoID;
  };
  struct dbCountry_t
  {
    uint32_t seek;
    uint8_t id;
    float lat, lon;
    std::string iso, name_ru, name_en;
  };
  struct dbRegion_t
  {
    uint32_t seek;
    uint32_t id;
    uint16_t seekCountry;
    std::string iso, name_ru, name_en;
  };
  struct dbCity_t
  {
    uint32_t seek;
    uint32_t id, seekRegion;
    uint8_t idCountry;
    double lat, lon;
    std::string name_ru, name_en;
  };
  
  SxGeoParser_t(std::string db_filename);
  ~SxGeoParser_t();
  
  inline bool isError(){return err != errNone;}
  inline errCode_t error(){return err;}
  std::string errorString();
  
  errCode_t open_db(std::string & db_filename);
  
  dbHeader_t getDbHeader(){return dbHeader;}
  
  bool dbIsValid(){return flag_dbIsValid;}
  std::string dbVersionStr();
  std::string dbTypeStr();
  std::string dbEncodingStr();
  std::string dbCreateDate();
  
  std::vector<std::string> location_full(std::vector<std::string> addrs, const char * sep = ":");
  
private:
  errCode_t err;
  
  bool flag_dbIsValid;
  
  std::string dbFileName;
  
  dbHeader_t dbHeader;
  
  std::fpos_t seekIdx_first, seekIdx_main;  
  std::vector<uint32_t> dbIdx_first;
  std::vector<uint32_t> dbIdx_main;

  std::fpos_t seekRanges;
  std::vector<dbRange_t> dbRanges;

  std::fpos_t seekCountries, seekRegions, seekCities;
  std::vector<dbCountry_t> dbCountries;
  std::vector<dbRegion_t> dbRegions;
  std::vector<dbCity_t> dbCities;
  
  uint32_t geoIdByAddr(uint32_t addr, bool * ok = 0);
  
  dbCity_t cityBySeek(uint32_t id, bool * ok = 0);
  
  dbRegion_t regionBySeek(uint32_t seek, bool * ok);
  
  dbCountry_t countryById(uint8_t id, bool * ok = 0);
};

#endif /* SXGEOPARSER_H */

