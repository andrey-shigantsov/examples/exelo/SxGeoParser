/* 
 * File:   dbRead.h
 */

#ifndef SXGEOPARSER_DBREAD_H
#define SXGEOPARSER_DBREAD_H

#include "../SxGeoParser.h"

#include <boost/endian/buffers.hpp>

using namespace boost::endian;

template <typename T>
static inline bool db_read_buf(std::FILE * db, T * buf)
{
  size_t n = std::fread(buf, sizeof(T), 1, db);
  return n == 1;
}
template <typename T, order Order, int8_t bitsCount>
static inline SxGeoParser_t::errCode_t db_read(std::FILE * db, T & data)
{
  endian_buffer<Order, T, bitsCount> buf(0);
  if (!db_read_buf(db, &buf))
    return SxGeoParser_t::errRead;
  data = buf.value();
  return SxGeoParser_t::errNone;
}

template <typename T>
static inline SxGeoParser_t::errCode_t db_read(std::FILE * db, T & data)
{
  return db_read<T,sizeof(T)*8>(db, data);
}

static inline SxGeoParser_t::errCode_t db_read(std::FILE * db, char * data, std::size_t size)
{
  size_t n = std::fread(data, 1, size, db);
  if (n != size)
    return SxGeoParser_t::errRead;
  return SxGeoParser_t::errNone;
}

static inline SxGeoParser_t::errCode_t check_dbId(std::FILE * db, bool & isValid)
{
  SxGeoParser_t::errCode_t res;
  char fileId[3];
  res = db_read(db, fileId, sizeof(fileId));
  if (res)
    return res;
  
  isValid = strncmp(fileId, "SxG", 3) == 0;
  if (isValid)
    return SxGeoParser_t::errNone;
  else
    return SxGeoParser_t::errCheck_dbId;
}

#include <boost/any.hpp>
typedef boost::any SxGeoUniFormatVariant_t;

template <typename T>
static inline T getDbUniVal(SxGeoUniFormatVariant_t x, bool * ok)
{
  T y;
  try
  {
    y = boost::any_cast<T>(x);
    *ok = true;
  }
  catch (const boost::bad_any_cast &ec)
  {
    *ok = false;
  }
  return y;
}

template <typename T, int8_t bitsCount>
static inline SxGeoParser_t::errCode_t db_read(std::FILE * db, SxGeoUniFormatVariant_t & data)
{
  SxGeoParser_t::errCode_t res;
  
  T x;
  res = db_read<T,order::little,bitsCount>(db, x);
  data = x;
  
  return res;
}

#endif /* SXGEOPARSER_DBREAD_H */
